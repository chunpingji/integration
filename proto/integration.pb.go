// Code generated by protoc-gen-go. DO NOT EDIT.
// versions:
// 	protoc-gen-go v1.23.0
// 	protoc        v3.12.4
// source: integration.proto

package proto

import (
	context "context"
	proto "github.com/golang/protobuf/proto"
	grpc "google.golang.org/grpc"
	codes "google.golang.org/grpc/codes"
	status "google.golang.org/grpc/status"
	protoreflect "google.golang.org/protobuf/reflect/protoreflect"
	protoimpl "google.golang.org/protobuf/runtime/protoimpl"
	reflect "reflect"
	sync "sync"
)

const (
	// Verify that this generated code is sufficiently up-to-date.
	_ = protoimpl.EnforceVersion(20 - protoimpl.MinVersion)
	// Verify that runtime/protoimpl is sufficiently up-to-date.
	_ = protoimpl.EnforceVersion(protoimpl.MaxVersion - 20)
)

// This is a compile-time assertion that a sufficiently up-to-date version
// of the legacy proto package is being used.
const _ = proto.ProtoPackageIsVersion4

type RegistPlatformRequest struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	PlatformId          string `protobuf:"bytes,1,opt,name=platformId,proto3" json:"platformId,omitempty"`
	PlatformName        string `protobuf:"bytes,2,opt,name=platformName,proto3" json:"platformName,omitempty"`
	PlatformDescription string `protobuf:"bytes,3,opt,name=platformDescription,proto3" json:"platformDescription,omitempty"`
	Version             string `protobuf:"bytes,4,opt,name=version,proto3" json:"version,omitempty"`
	TenantId            string `protobuf:"bytes,5,opt,name=tenantId,proto3" json:"tenantId,omitempty"`
}

func (x *RegistPlatformRequest) Reset() {
	*x = RegistPlatformRequest{}
	if protoimpl.UnsafeEnabled {
		mi := &file_integration_proto_msgTypes[0]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *RegistPlatformRequest) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*RegistPlatformRequest) ProtoMessage() {}

func (x *RegistPlatformRequest) ProtoReflect() protoreflect.Message {
	mi := &file_integration_proto_msgTypes[0]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use RegistPlatformRequest.ProtoReflect.Descriptor instead.
func (*RegistPlatformRequest) Descriptor() ([]byte, []int) {
	return file_integration_proto_rawDescGZIP(), []int{0}
}

func (x *RegistPlatformRequest) GetPlatformId() string {
	if x != nil {
		return x.PlatformId
	}
	return ""
}

func (x *RegistPlatformRequest) GetPlatformName() string {
	if x != nil {
		return x.PlatformName
	}
	return ""
}

func (x *RegistPlatformRequest) GetPlatformDescription() string {
	if x != nil {
		return x.PlatformDescription
	}
	return ""
}

func (x *RegistPlatformRequest) GetVersion() string {
	if x != nil {
		return x.Version
	}
	return ""
}

func (x *RegistPlatformRequest) GetTenantId() string {
	if x != nil {
		return x.TenantId
	}
	return ""
}

type RegistPlatformResponse struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Code    int32  `protobuf:"varint,1,opt,name=code,proto3" json:"code,omitempty"`
	Message string `protobuf:"bytes,2,opt,name=message,proto3" json:"message,omitempty"`
}

func (x *RegistPlatformResponse) Reset() {
	*x = RegistPlatformResponse{}
	if protoimpl.UnsafeEnabled {
		mi := &file_integration_proto_msgTypes[1]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *RegistPlatformResponse) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*RegistPlatformResponse) ProtoMessage() {}

func (x *RegistPlatformResponse) ProtoReflect() protoreflect.Message {
	mi := &file_integration_proto_msgTypes[1]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use RegistPlatformResponse.ProtoReflect.Descriptor instead.
func (*RegistPlatformResponse) Descriptor() ([]byte, []int) {
	return file_integration_proto_rawDescGZIP(), []int{1}
}

func (x *RegistPlatformResponse) GetCode() int32 {
	if x != nil {
		return x.Code
	}
	return 0
}

func (x *RegistPlatformResponse) GetMessage() string {
	if x != nil {
		return x.Message
	}
	return ""
}

type EntitiesRequest struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields
}

func (x *EntitiesRequest) Reset() {
	*x = EntitiesRequest{}
	if protoimpl.UnsafeEnabled {
		mi := &file_integration_proto_msgTypes[2]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *EntitiesRequest) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*EntitiesRequest) ProtoMessage() {}

func (x *EntitiesRequest) ProtoReflect() protoreflect.Message {
	mi := &file_integration_proto_msgTypes[2]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use EntitiesRequest.ProtoReflect.Descriptor instead.
func (*EntitiesRequest) Descriptor() ([]byte, []int) {
	return file_integration_proto_rawDescGZIP(), []int{2}
}

type EntitiesResponse struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields
}

func (x *EntitiesResponse) Reset() {
	*x = EntitiesResponse{}
	if protoimpl.UnsafeEnabled {
		mi := &file_integration_proto_msgTypes[3]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *EntitiesResponse) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*EntitiesResponse) ProtoMessage() {}

func (x *EntitiesResponse) ProtoReflect() protoreflect.Message {
	mi := &file_integration_proto_msgTypes[3]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use EntitiesResponse.ProtoReflect.Descriptor instead.
func (*EntitiesResponse) Descriptor() ([]byte, []int) {
	return file_integration_proto_rawDescGZIP(), []int{3}
}

var File_integration_proto protoreflect.FileDescriptor

var file_integration_proto_rawDesc = []byte{
	0x0a, 0x11, 0x69, 0x6e, 0x74, 0x65, 0x67, 0x72, 0x61, 0x74, 0x69, 0x6f, 0x6e, 0x2e, 0x70, 0x72,
	0x6f, 0x74, 0x6f, 0x12, 0x05, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x22, 0xc3, 0x01, 0x0a, 0x15, 0x52,
	0x65, 0x67, 0x69, 0x73, 0x74, 0x50, 0x6c, 0x61, 0x74, 0x66, 0x6f, 0x72, 0x6d, 0x52, 0x65, 0x71,
	0x75, 0x65, 0x73, 0x74, 0x12, 0x1e, 0x0a, 0x0a, 0x70, 0x6c, 0x61, 0x74, 0x66, 0x6f, 0x72, 0x6d,
	0x49, 0x64, 0x18, 0x01, 0x20, 0x01, 0x28, 0x09, 0x52, 0x0a, 0x70, 0x6c, 0x61, 0x74, 0x66, 0x6f,
	0x72, 0x6d, 0x49, 0x64, 0x12, 0x22, 0x0a, 0x0c, 0x70, 0x6c, 0x61, 0x74, 0x66, 0x6f, 0x72, 0x6d,
	0x4e, 0x61, 0x6d, 0x65, 0x18, 0x02, 0x20, 0x01, 0x28, 0x09, 0x52, 0x0c, 0x70, 0x6c, 0x61, 0x74,
	0x66, 0x6f, 0x72, 0x6d, 0x4e, 0x61, 0x6d, 0x65, 0x12, 0x30, 0x0a, 0x13, 0x70, 0x6c, 0x61, 0x74,
	0x66, 0x6f, 0x72, 0x6d, 0x44, 0x65, 0x73, 0x63, 0x72, 0x69, 0x70, 0x74, 0x69, 0x6f, 0x6e, 0x18,
	0x03, 0x20, 0x01, 0x28, 0x09, 0x52, 0x13, 0x70, 0x6c, 0x61, 0x74, 0x66, 0x6f, 0x72, 0x6d, 0x44,
	0x65, 0x73, 0x63, 0x72, 0x69, 0x70, 0x74, 0x69, 0x6f, 0x6e, 0x12, 0x18, 0x0a, 0x07, 0x76, 0x65,
	0x72, 0x73, 0x69, 0x6f, 0x6e, 0x18, 0x04, 0x20, 0x01, 0x28, 0x09, 0x52, 0x07, 0x76, 0x65, 0x72,
	0x73, 0x69, 0x6f, 0x6e, 0x12, 0x1a, 0x0a, 0x08, 0x74, 0x65, 0x6e, 0x61, 0x6e, 0x74, 0x49, 0x64,
	0x18, 0x05, 0x20, 0x01, 0x28, 0x09, 0x52, 0x08, 0x74, 0x65, 0x6e, 0x61, 0x6e, 0x74, 0x49, 0x64,
	0x22, 0x46, 0x0a, 0x16, 0x52, 0x65, 0x67, 0x69, 0x73, 0x74, 0x50, 0x6c, 0x61, 0x74, 0x66, 0x6f,
	0x72, 0x6d, 0x52, 0x65, 0x73, 0x70, 0x6f, 0x6e, 0x73, 0x65, 0x12, 0x12, 0x0a, 0x04, 0x63, 0x6f,
	0x64, 0x65, 0x18, 0x01, 0x20, 0x01, 0x28, 0x05, 0x52, 0x04, 0x63, 0x6f, 0x64, 0x65, 0x12, 0x18,
	0x0a, 0x07, 0x6d, 0x65, 0x73, 0x73, 0x61, 0x67, 0x65, 0x18, 0x02, 0x20, 0x01, 0x28, 0x09, 0x52,
	0x07, 0x6d, 0x65, 0x73, 0x73, 0x61, 0x67, 0x65, 0x22, 0x11, 0x0a, 0x0f, 0x45, 0x6e, 0x74, 0x69,
	0x74, 0x69, 0x65, 0x73, 0x52, 0x65, 0x71, 0x75, 0x65, 0x73, 0x74, 0x22, 0x12, 0x0a, 0x10, 0x45,
	0x6e, 0x74, 0x69, 0x74, 0x69, 0x65, 0x73, 0x52, 0x65, 0x73, 0x70, 0x6f, 0x6e, 0x73, 0x65, 0x32,
	0x5b, 0x0a, 0x10, 0x52, 0x65, 0x67, 0x69, 0x73, 0x74, 0x65, 0x72, 0x50, 0x6c, 0x61, 0x74, 0x66,
	0x6f, 0x72, 0x6d, 0x12, 0x47, 0x0a, 0x06, 0x52, 0x65, 0x67, 0x69, 0x73, 0x74, 0x12, 0x1c, 0x2e,
	0x70, 0x72, 0x6f, 0x74, 0x6f, 0x2e, 0x52, 0x65, 0x67, 0x69, 0x73, 0x74, 0x50, 0x6c, 0x61, 0x74,
	0x66, 0x6f, 0x72, 0x6d, 0x52, 0x65, 0x71, 0x75, 0x65, 0x73, 0x74, 0x1a, 0x1d, 0x2e, 0x70, 0x72,
	0x6f, 0x74, 0x6f, 0x2e, 0x52, 0x65, 0x67, 0x69, 0x73, 0x74, 0x50, 0x6c, 0x61, 0x74, 0x66, 0x6f,
	0x72, 0x6d, 0x52, 0x65, 0x73, 0x70, 0x6f, 0x6e, 0x73, 0x65, 0x22, 0x00, 0x42, 0x09, 0x5a, 0x07,
	0x2e, 0x3b, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x62, 0x06, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x33,
}

var (
	file_integration_proto_rawDescOnce sync.Once
	file_integration_proto_rawDescData = file_integration_proto_rawDesc
)

func file_integration_proto_rawDescGZIP() []byte {
	file_integration_proto_rawDescOnce.Do(func() {
		file_integration_proto_rawDescData = protoimpl.X.CompressGZIP(file_integration_proto_rawDescData)
	})
	return file_integration_proto_rawDescData
}

var file_integration_proto_msgTypes = make([]protoimpl.MessageInfo, 4)
var file_integration_proto_goTypes = []interface{}{
	(*RegistPlatformRequest)(nil),  // 0: proto.RegistPlatformRequest
	(*RegistPlatformResponse)(nil), // 1: proto.RegistPlatformResponse
	(*EntitiesRequest)(nil),        // 2: proto.EntitiesRequest
	(*EntitiesResponse)(nil),       // 3: proto.EntitiesResponse
}
var file_integration_proto_depIdxs = []int32{
	0, // 0: proto.RegisterPlatform.Regist:input_type -> proto.RegistPlatformRequest
	1, // 1: proto.RegisterPlatform.Regist:output_type -> proto.RegistPlatformResponse
	1, // [1:2] is the sub-list for method output_type
	0, // [0:1] is the sub-list for method input_type
	0, // [0:0] is the sub-list for extension type_name
	0, // [0:0] is the sub-list for extension extendee
	0, // [0:0] is the sub-list for field type_name
}

func init() { file_integration_proto_init() }
func file_integration_proto_init() {
	if File_integration_proto != nil {
		return
	}
	if !protoimpl.UnsafeEnabled {
		file_integration_proto_msgTypes[0].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*RegistPlatformRequest); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_integration_proto_msgTypes[1].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*RegistPlatformResponse); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_integration_proto_msgTypes[2].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*EntitiesRequest); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_integration_proto_msgTypes[3].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*EntitiesResponse); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
	}
	type x struct{}
	out := protoimpl.TypeBuilder{
		File: protoimpl.DescBuilder{
			GoPackagePath: reflect.TypeOf(x{}).PkgPath(),
			RawDescriptor: file_integration_proto_rawDesc,
			NumEnums:      0,
			NumMessages:   4,
			NumExtensions: 0,
			NumServices:   1,
		},
		GoTypes:           file_integration_proto_goTypes,
		DependencyIndexes: file_integration_proto_depIdxs,
		MessageInfos:      file_integration_proto_msgTypes,
	}.Build()
	File_integration_proto = out.File
	file_integration_proto_rawDesc = nil
	file_integration_proto_goTypes = nil
	file_integration_proto_depIdxs = nil
}

// Reference imports to suppress errors if they are not otherwise used.
var _ context.Context
var _ grpc.ClientConnInterface

// This is a compile-time assertion to ensure that this generated file
// is compatible with the grpc package it is being compiled against.
const _ = grpc.SupportPackageIsVersion6

// RegisterPlatformClient is the client API for RegisterPlatform service.
//
// For semantics around ctx use and closing/ending streaming RPCs, please refer to https://godoc.org/google.golang.org/grpc#ClientConn.NewStream.
type RegisterPlatformClient interface {
	Regist(ctx context.Context, in *RegistPlatformRequest, opts ...grpc.CallOption) (*RegistPlatformResponse, error)
}

type registerPlatformClient struct {
	cc grpc.ClientConnInterface
}

func NewRegisterPlatformClient(cc grpc.ClientConnInterface) RegisterPlatformClient {
	return &registerPlatformClient{cc}
}

func (c *registerPlatformClient) Regist(ctx context.Context, in *RegistPlatformRequest, opts ...grpc.CallOption) (*RegistPlatformResponse, error) {
	out := new(RegistPlatformResponse)
	err := c.cc.Invoke(ctx, "/proto.RegisterPlatform/Regist", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

// RegisterPlatformServer is the server API for RegisterPlatform service.
type RegisterPlatformServer interface {
	Regist(context.Context, *RegistPlatformRequest) (*RegistPlatformResponse, error)
}

// UnimplementedRegisterPlatformServer can be embedded to have forward compatible implementations.
type UnimplementedRegisterPlatformServer struct {
}

func (*UnimplementedRegisterPlatformServer) Regist(context.Context, *RegistPlatformRequest) (*RegistPlatformResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method Regist not implemented")
}

func RegisterRegisterPlatformServer(s *grpc.Server, srv RegisterPlatformServer) {
	s.RegisterService(&_RegisterPlatform_serviceDesc, srv)
}

func _RegisterPlatform_Regist_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(RegistPlatformRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(RegisterPlatformServer).Regist(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/proto.RegisterPlatform/Regist",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(RegisterPlatformServer).Regist(ctx, req.(*RegistPlatformRequest))
	}
	return interceptor(ctx, in, info, handler)
}

var _RegisterPlatform_serviceDesc = grpc.ServiceDesc{
	ServiceName: "proto.RegisterPlatform",
	HandlerType: (*RegisterPlatformServer)(nil),
	Methods: []grpc.MethodDesc{
		{
			MethodName: "Regist",
			Handler:    _RegisterPlatform_Regist_Handler,
		},
	},
	Streams:  []grpc.StreamDesc{},
	Metadata: "integration.proto",
}
