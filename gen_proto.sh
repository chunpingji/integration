#!/bin/bash

protoc -I proto/ --go_out=plugins=grpc:proto/ integration.proto
protoc -I proto/ --go_out=plugins=grpc:proto/ simple.proto
